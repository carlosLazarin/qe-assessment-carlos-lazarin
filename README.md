# Quality Engineering Assessment Project

This project is the baseline for creating your own automation assessment project.

## General Instructions

1. Fork the repository (https://bitbucket.org/leonardonelson/qe-assessment/fork). For more information on forking, take a look at this [page](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html).
2. Complete the exercises
3. Once you are done, create a Pull Request to leonardonelson/master
4. The instructors will validate the assessment by the PR created 

#### Run Instructions:
1. Once you cloned your forked repo, do a mvn clean install in order to install all the maven dependencies
2. Open AllStories.java, right-click on it, run as JUnit test.
3. Your automation suite should start executing and Chrome should be opened