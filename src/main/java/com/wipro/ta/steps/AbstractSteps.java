package com.wipro.ta.steps;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.wipro.ta.context.TestContext;

public abstract class AbstractSteps {

	protected Logger LOG = Logger.getLogger(this.getClass());

	protected static final String PRODUCT_QUANTITY_CONTEXT_KEY = "productQuantity";

	@Autowired
	protected TestContext context;
}
