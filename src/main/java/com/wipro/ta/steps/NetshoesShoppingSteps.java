package com.wipro.ta.steps;

import com.wipro.ta.context.TestContext;
import com.wipro.ta.pages.ProductDetailsPage;
import com.wipro.ta.pages.SearchResultsPage;
import com.wipro.ta.pages.ShoppingCartPage;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 *
 * @uthor Lazarin, Carlos
 * @since 06-18-2018
 *
 */
@Component
public class NetshoesShoppingSteps extends AbstractSteps {

    @Autowired
    private SearchResultsPage searchResultsPage;

    @Autowired
    private TestContext testContext;

    @Autowired
    private ShoppingCartPage shoppingCartPage;

    @Autowired
    private ProductDetailsPage productDetailsPage;

    @When("the user adds $quantity product(s) in the shopping cart")
    public void addProductsInTheCart(@Named("quantity") String quantity){

        Integer productQuantity = Integer.valueOf(quantity);

        List<WebElement> searchResultItems = searchResultsPage.getSearchResultItems();

        if(!CollectionUtils.isEmpty(searchResultItems)
                && searchResultItems.size() > productQuantity){

            for(int i = 1; i <= productQuantity; i++){

                WebElement item = searchResultItems.get(i);
                searchResultsPage.moveMouseAndClickInTheQuickViewButton(item);

                productDetailsPage.selectProductSize();
                productDetailsPage.addProductInTheCart((productQuantity > 1) && (i != (productQuantity) ? true : false));

            }

        }else{
           LOG.error("Not possible to add "+quantity+" because there is not enough items in" +
                    " the search results - search results amount: "+searchResultItems == null ? "null" : searchResultItems.size());
        }

        if(testContext.getFromContext(PRODUCT_QUANTITY_CONTEXT_KEY) != null){
            testContext.remove(PRODUCT_QUANTITY_CONTEXT_KEY);
        }

        testContext.add(PRODUCT_QUANTITY_CONTEXT_KEY, productQuantity);

    }

    @Then("the shopping cart is displayed")
    public void displayShoppingCart(){
        assertNotNull("Shopping cart page not displayed",shoppingCartPage.getCartLabel().getText());
    }

    @Then("the shopping cart contains all products added by the user")
    public void checkIfShoppingCartContainsAllProducts(){

        Integer productQuantity = (Integer)testContext.getFromContext(PRODUCT_QUANTITY_CONTEXT_KEY);

        int totalProductsInCart = shoppingCartPage.verifyQuantityOfProductInTheCart();

        assertFalse("Cart does not have any added product",totalProductsInCart == 0);
        assertTrue("Cart does not have correct amount of previously added products - it should have "+productQuantity+" and it has " +totalProductsInCart+
                "!",productQuantity.equals(Integer.valueOf(totalProductsInCart)));
    }

    @Then("the total price should be the sum of the individual prices of each item")
    public void checkTotalPriceIsCorrect(){

        List<BigDecimal> individualProductsPriceList = this.shoppingCartPage.getIndividualProductsPriceList();

        BigDecimal referenceValue = BigDecimal.ZERO;

        for(BigDecimal individualPrice : individualProductsPriceList){
            referenceValue = referenceValue.add(individualPrice);
        }

        BigDecimal cartTotalAmount = this.shoppingCartPage.getCartTotalAmount();

        assertTrue("Total price displayed is incorrect - it should be "+referenceValue+" and it is "+cartTotalAmount+"!",referenceValue.equals(cartTotalAmount));
    }

    @Then("the user informs the CEP $cepNumber in the 'Frete' field")
    public void userInformHisCepNumber(@Named("cepNumber") String cep){
        this.shoppingCartPage.fillUpCepNumber(cep);
    }

    @When("the user clicks on 'the shopping card icon'")
    public void clicksInTheShoppingCartIcon(){
         productDetailsPage.clickInTheCartIcon();
    }

    @Then("the user clicks on 'Calcular Frete'")
    public void clicksToCalculateShippingPrice(){
        this.shoppingCartPage.calculateShipping();
    }

    @Then("all added items are removed from the shopping cart")
    public void checkIfItemsWereRemovedFromCart(){
         assertTrue("Cart should be empty but it is not!",this.shoppingCartPage.checkIfCartIsEmpty());
    }

    @Then("the shipping cost 'FRETE GRÁTIS' should be displayed to the user")
    public void verifyIfShippingCostLabelIsShown(){
        String shippingValueLabel = this.shoppingCartPage.getShippingValueLabel();
        assertEquals("'FRETE GRÁTIS' label is not shown", "FRETE GRÁTIS"  ,shippingValueLabel);
    }

    @When("the user clicks on 'Limpar Carrinho'")
    public void clickCleanCartButton(){

        Integer productQuantity = (Integer) testContext.getFromContext(PRODUCT_QUANTITY_CONTEXT_KEY);

        if(productQuantity != null && productQuantity != 0){
            this.shoppingCartPage.cleanCart();
        }

    }

}