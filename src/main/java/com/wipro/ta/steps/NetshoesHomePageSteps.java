package com.wipro.ta.steps;

import com.wipro.ta.pages.HomePage;
import com.wipro.ta.pages.SearchResultsPage;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * @uthor Lazarin, Carlos
 * @since 06-18-2018
 */
@Component
public class NetshoesHomePageSteps extends AbstractSteps {

    @Autowired
    private HomePage homePage;

    @Autowired
    private SearchResultsPage searchResultsPage;

    @Given("the user access the NetShoes home page")
    public void givenCustomerAccessHomePage() {
        homePage.goToHomePageURL();
    }

    @Then("the products content should be displayed")
    public void thenProductListIsDisplayed() {
        WebElement contentDiv = homePage.getProductsContent();
        assertNotNull("Product content div not found in the screen", contentDiv);
        if (contentDiv != null) {
            Assert.assertTrue("The available products list was expected to be displayed, but it was not.", contentDiv.isDisplayed());
        }
    }

    @Given("the user clicks on 'the shopping card icon'")
    public void clicksInTheShoppingCartIcon() {
        this.homePage.clickInTheCartIcon();
    }

    @When("the user searches for the product $searchTerm")
    public void userSearchesForProductByBrandName(@Named("searchTerm") String searchTerm) {
        this.homePage.searchProductsByGivenString(searchTerm);
    }

    @Then("the search results are displayed")
    public void displaySearchResults() {
        List<WebElement> searchResults = this.searchResultsPage.getSearchResultItems();
        assertNotNull("Search result is null - it should not be!", searchResults);
    }

    @Then("only items corresponding to the search term $searchTerm are displayed")
    public void checkItemCorrespondingToSearchTerm(@Named("searchTerm") String searchTerm) {
        List<WebElement> searchResults = this.searchResultsPage.getSearchResultItems();
        boolean searchTermControl = true;
        for (WebElement webElement : searchResults) {
            WebElement itemAnchor = webElement.findElement(By.xpath("//a[@class='n card-link']/span[@itemprop='name']"));
            if (itemAnchor != null) {
                String productDescription = itemAnchor.getText();
                Pattern pattern = Pattern.compile("/(" + searchTerm + ")+");
                Matcher matcher = pattern.matcher(productDescription);
                if (matcher.groupCount() == 0) {
                    searchTermControl = false;
                    break;
                }
            }
        }
        assertTrue("Result search does not have matching products!", searchTermControl);
    }

    @Then("more than $quantity items should be displayed in the result")
    public void checkSearchResultsQuantity(@Named("$quantity") String quantity) {
        List<WebElement> searchResults = this.searchResultsPage.getSearchResultItems();
        assertNotNull(searchResults);
        assertTrue("Result search does not bring more results than specified quantity", searchResults.size() > Integer.valueOf(quantity));
    }
}