package com.wipro.ta.pages;

import com.wipro.ta.configuration.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.List;

/**
 *
 * @uthor Lazarin, Carlos
 * @since 06-18-2018
 *
 */
@PageObject
public class SearchResultsPage extends AbstractPage {

    @FindAll( @FindBy(xpath = "//div[@qa-automation='search-itens']") )
    private List<WebElement> searchResultItems;

    @FindBy(xpath = HOME_CART_BUTTON_X_PATH_SELECTOR)
    private WebElement cartIcon;

    public List<WebElement> getSearchResultItems(){
        return this.searchResultItems;
    }

    public void moveMouseAndClickInTheQuickViewButton(WebElement webElement){
        WebElement productLink = webElement.findElement(By.xpath("//a[@class='i card-link']"));
        productLink.click();
    }
}
