package com.wipro.ta.pages;

import com.wipro.ta.configuration.PageObject;
import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.springframework.beans.factory.annotation.Value;

/**
 *
 * @uthor Lazarin, Carlos
 * @since 06-18-2018
 *
 */
@PageObject
public class HomePage extends AbstractPage {

    @Value("${home.url}")
    private String NETSHOES_HOMEPAGE_URL;

    @FindBy(className = "content")
    private WebElement contentDiv;

    @FindBy(id = "search-input")
    private WebElement inputSearch;

    @FindBy(xpath = "//button[@qa-automation='home-search-button']")
    private WebElement searchButton;

    @FindBy(xpath = HOME_CART_BUTTON_X_PATH_SELECTOR)
    private WebElement cartIcon;

    @FindBy(xpath = "//span[@class='cart-count-badge']/span")
    private WebElement cartAmountItems;

    public void goToHomePageURL(){
        LOG.info("Navigating user to page: " + NETSHOES_HOMEPAGE_URL);
        webDriverProvider.get().get(NETSHOES_HOMEPAGE_URL);
        getWebDriverWait(15).until(ExpectedConditions.visibilityOfElementLocated(By.className("home-killers-carousel")));//wait a lit  bit it loads
    }

    public WebElement getProductsContent(){
        return contentDiv;
    }

    /**
     *
     * @param searchString
     * @return {@link HomePage}
     */
    public void searchProductsByGivenString(String searchString){
        if(!StringUtils.isEmpty(searchString)){
            LOG.info("Searching products by given string="+searchString);
            this.inputSearch.clear();
            this.inputSearch.sendKeys(searchString);
            getWebDriverWait(20).until(ExpectedConditions.elementToBeClickable(searchButton));
            this.searchButton.click();
        }else{
            LOG.warn("Not possible to search products by given string because parameter it is null or empty");
        }
    }

    public void clickInTheCartIcon() {
        LOG.info("HomePage - Clicking in the cart icon.");
        this.cartIcon.click();
    }
}