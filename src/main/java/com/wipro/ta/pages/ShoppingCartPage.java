package com.wipro.ta.pages;

import com.wipro.ta.configuration.PageObject;
import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.springframework.beans.factory.annotation.Value;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 *
 * @uthor Lazarin, Carlos
 * @since 06-18-2018
 *
 */
@PageObject
public class ShoppingCartPage extends AbstractPage{

    @FindBy(className = "table-products")
    private WebElement productsTable;

    @FindBy(className = "table-cart-data")
    private WebElement cartDataTable;

    @FindBy(className = "cart-title")
    private WebElement cartLabel;

    @FindBy(xpath = "//p[@qa-automation='cart-price']")
    private WebElement cartTotalAmount;

    @FindBy(xpath = "//input[@qa-automation='cart-cep-field']")
    private WebElement shippingInputText;

    @FindBy(xpath = "//button[@qa-automation='cart-cep-button']")
    private WebElement calculateShippingButton;

    @FindBy(xpath = "//strong[@class='cart-free-shipping-text']")
    private WebElement shippingValueLabel;

    @FindBy(xpath = "//a[@qa-automation='cart-erase-cart-button']")
    private WebElement cleanCartLink;

    @Value("${cart.url}")
    private String NETSHOES_CART_URL;

    public void fillUpCepNumber(String cepNumber){
        LOG.info("Filling up cep number..."+cepNumber);
        waitForTableProductsBeLocated();
        this.shippingInputText.sendKeys(cepNumber);
    }

    public void calculateShipping(){
        LOG.info("Calculating shipping...");
        this.calculateShippingButton.click();
    }

    public void goToCartURL(){
        LOG.info("Navigating user to page: " + NETSHOES_CART_URL);

        if(!webDriverProvider.get().getCurrentUrl().equals(NETSHOES_CART_URL)){
            webDriverProvider.get().get(NETSHOES_CART_URL);
            waitPageLoad();
        }

    }

    public WebElement getCartLabel(){
        return cartLabel;
    }

    private void waitForTableProductsBeLocated(){
        getWebDriverWait(20).until(ExpectedConditions.visibilityOfElementLocated(By.className("table-cart-data")));
    }

    public BigDecimal getCartTotalAmount(){
        waitForTableProductsBeLocated();
        try {
            return parsePriceFormattedStringToBigDecimal(cartTotalAmount.getText(), new Locale("pt","BR"));
        } catch (ParseException e) {
            LOG.error("An error happened tyring to parse cart total amount to a BigDecimal value - cart amount text: "+cartTotalAmount.getText());
        }
        return BigDecimal.ZERO;
    }

    public List<BigDecimal> getIndividualProductsPriceList(){

        waitForTableProductsBeLocated();

        LOG.info("Checking individual product price");

        List<BigDecimal> individualProductPriceList = new ArrayList<>();

        List<WebElement> productPriceList = productsTable.findElements(By.xpath("//p[@qa-automation='cart-product-price']"));
        for(WebElement productPrice : productPriceList){
            try {
                individualProductPriceList.add(parsePriceFormattedStringToBigDecimal(productPrice.getText(), new Locale("pt","BR")));
            } catch (ParseException e) {
                LOG.error("An error happened tyring to parse an given product price to a BigDecimal value - price text: "+productPrice.getText());
            }
        }
        return individualProductPriceList;
    }

    private BigDecimal parsePriceFormattedStringToBigDecimal(final String amount, final Locale locale) throws ParseException {
        final NumberFormat format = NumberFormat.getNumberInstance(locale);
        if (format instanceof DecimalFormat) {
            ((DecimalFormat) format).setParseBigDecimal(true);
        }
        return (BigDecimal) format.parse(amount.replaceAll("[^\\d.,]",""));
    }

    public int verifyQuantityOfProductInTheCart(){

        waitForTableProductsBeLocated();

        int productCount = 0;
        List<WebElement> productQuantityList = productsTable.findElements(By.xpath("//input[@qa-automation='cart-product-qty']"));
        for(WebElement productQuantity : productQuantityList){

            String productQuantityValue = productQuantity.getAttribute("value");
            productCount = productCount + Integer.parseInt(productQuantityValue);

        }

        return productCount;
    }

    public void cleanCart(){
        if(!checkIfCartIsEmpty()){
            LOG.info("Cleaning cart...");
            this.cleanCartLink.click();
        }
    }

    public String getShippingValueLabel(){

        LOG.info("Returning shipping label...");

        waitForTableProductsBeLocated();

        getWebDriverWait(30).until(ExpectedConditions.attributeContains(By.className("ns-loading"), "class", "hide"));
        String shippingLabel = StringUtils.EMPTY;

        if(this.shippingValueLabel != null){
            shippingLabel =  this.shippingValueLabel.getText();
        }

        LOG.info("Shipping label..."+shippingLabel);

        return shippingLabel;
    }

    public boolean checkIfCartIsEmpty(){
        WebElement emptLabel = null;
        try {
            emptLabel = cartDataTable.findElement(By.xpath("//div[@class='empty-cart-holder']/div[@class='empty-cart']/h2[@class='title']"));
        }catch(Exception e){
            LOG.warn("Cart is not empty!!");
            return false;
        }
        if(emptLabel != null){
            return emptLabel.getText().equals("Seu carrinho está vazio");
        }
        return false;
    }
}