package com.wipro.ta.pages;

import com.wipro.ta.configuration.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * Created by e047386 on 6/25/2018.
 */
@PageObject
public class ProductDetailsPage extends AbstractPage {

    private static final String PRODUCT_SIZE_X_PATH_SELECTOR = "//a[@qa-automation='product-size']";
    private static final String BUY_BUTTON_X_PATH_SELECTOR = "//button[@id='buy-button-now']";

    @FindAll( @FindBy(xpath = PRODUCT_SIZE_X_PATH_SELECTOR) )
    private List<WebElement> productSizeOptions;

    @FindBy(xpath = BUY_BUTTON_X_PATH_SELECTOR)
    private WebElement buyNowButton;

    @FindBy(xpath = HOME_CART_BUTTON_X_PATH_SELECTOR)
    private WebElement cartIcon;

    public void addProductInTheCart(boolean comeBackToSearchResults){
        LOG.info("Adding product in the cart ....");
        getWebDriverWait(20).until(ExpectedConditions.elementToBeClickable(By.xpath(BUY_BUTTON_X_PATH_SELECTOR)));
        this.buyNowButton.click();
        LOG.info("addProductInTheCart - New product added in cart!!");
        if(comeBackToSearchResults) {
            LOG.info("Coming back to searching results page");
            this.goBack();
            this.goBack();
        }
    }

    public void selectProductSize(){

        LOG.info("Selecting product size...");
        getWebDriverWait(30).until(ExpectedConditions.elementToBeClickable(By.xpath(PRODUCT_SIZE_X_PATH_SELECTOR)));

        List<WebElement> sizeOptions = productSizeOptions;

        if(!CollectionUtils.isEmpty(sizeOptions)){
            sizeOptions.get(0).click();
            LOG.info("Product size selected ....");
        }else{
            LOG.warn("addProductInTheCart - Not possible to select product size!!");
        }

    }

    public void clickInTheCartIcon(){
        LOG.info("SearchResultsPage - Clicking in the cart icon.");
        getWebDriverWait(30).until(ExpectedConditions.elementToBeClickable(By.xpath(HOME_CART_BUTTON_X_PATH_SELECTOR)));
        this.cartIcon.click();
    }
}