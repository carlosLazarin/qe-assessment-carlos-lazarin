Narrative:
As a system user
I want to perform an some operations related to shopping
So that I can be able to manage my shopping cart

Meta: @buying

Lifecycle:
After:
Scope: STORY
Outcome: ANY
Given the user clicks on 'the shopping card icon'
Then the shopping cart is displayed
When the user clicks on 'Limpar Carrinho'
Then all added items are removed from the shopping cart

Scenario: The customer should be able to add items in the shopping cart
Given the user access the NetShoes home page
When the user searches for the product <brand>
Then the search results are displayed
When the user adds <quantity> product(s) in the shopping cart
Then the shopping cart is displayed
And the shopping cart contains all products added by the user
And the total price should be the sum of the individual prices of each item

Examples:
| brand | quantity |
| Nike  | 2        |

Scenario: The customer should be able to calculate the shipping cost in the shopping cart
Given the user access the NetShoes home page
When the user searches for the product <brand>
Then the search results are displayed
When the user adds <quantity> product(s) in the shopping cart
Then the shopping cart is displayed
And the user informs the CEP <cepNumber> in the 'Frete' field
And the user clicks on 'Calcular Frete'
Then the shipping cost 'FRETE GRÁTIS' should be displayed to the user

Examples:
| brand | quantity | cepNumber |
| Nike  | 1        | 83035350  |

Scenario: The customer should be able to clear the shopping cart
Given the user access the NetShoes home page
When the user searches for the product <brand>
Then the search results are displayed
When the user adds <quantity> product(s) in the shopping cart
Then the shopping cart is displayed
And the shopping cart contains all products added by the user
When the user clicks on 'Limpar Carrinho'
Then all added items are removed from the shopping cart

Examples:
| brand | quantity |
| Nike  | 1        |