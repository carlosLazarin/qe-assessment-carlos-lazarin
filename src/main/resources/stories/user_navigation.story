Meta: @navigation

Narrative:
In order to buy sporting goods
As a customer
I want to be able to access and navigate in the web store

Scenario: The user should be able to navigate to the web store home page
Given the user access the NetShoes home page
Then the products content should be displayed