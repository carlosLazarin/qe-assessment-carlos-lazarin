Meta: @signup

Narrative:
As an not registered user
I want to sign up in the Netshoes
So that I can achieve my business goal

Scenario: user wants to sign up in the Netshoes website
Given an user not registered in the system
When the user clicks in the 'Login' link displayed as subitem of 'Entrar' one
Then system redirects user to login page
When user informs his e-mail address under 'Criar Conta' form
And user clicks in 'Prosseguir' button
Then system should display a screen requesting user's personal information
When user fills up all required fields in the form
And user clicks in the 'Continuar' button
Then user should be able to finish his registration
