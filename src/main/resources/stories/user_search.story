Meta: @search

Narrative:
As a user
I want to perform a search by product's brand name
So that I can achieve a business goal

Scenario: The customer should be able to search for specific brands and products
Given the user access the NetShoes home page
When the user searches for the product <search-term>
Then the search results are displayed
And only items corresponding to the search term <searchTerm> are displayed

Examples:
| searchTerm |
| Nike  |

Scenario: The product search result page should display more than <quantity> items
Given the user access the NetShoes home page
When the user searches for the product <searchTerm>
Then the search results are displayed
And more than <quantity> items should be displayed in the result

Examples:
| quantity | searchTerm |
| 5        | Nike  |